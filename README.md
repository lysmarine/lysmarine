

<img style="float: right;width:33%" src="img/56685673_864487050559490_2388214322967871488_n.png" >
<img style="float: right;width:33%" src="img/56196650_864487037226158_2555303931060158464_n.png" >
<img style="float: right;width:33%" src="img/55949750_864487103892818_7811739672480055296_n.png" >


<br>
<br>

# What is Lysmarine.
Lismarine is an operating system designed to be used as a navigation computer inside boats.
The software is build with pi-gen so it's use the same base as raspbian and it's compiled for raspberrypi.
It include software to read charts, Gribs, go on the internet and manage different inputs and outputs such as autopilots, GPS, AIS, depth sensors ....

# Hardware requirements.
In order to make the magic of Lysmarine append, you will need some physical devices to run your software on.
What you will need will vary a lot depending your inputs and outputs and mainly your geek level.

The minimum kit can be found on the [Minimal Hardware](doc/userdoc/hardware/minimal_hardware.md)


# Get Starded

&nbsp; &nbsp; &nbsp; &nbsp; [ Download and Install ](../install.md)

&nbsp; &nbsp; &nbsp; &nbsp; [ First run, What to do ](doc/userdoc/firstboot.md)

&nbsp; &nbsp; &nbsp; &nbsp; [ User documentation ](doc/userdoc/README.md)



# Developers
&nbsp; &nbsp; &nbsp; &nbsp; [ Project structure and contribution guidelines ](doc/contrib/README.md)

&nbsp; &nbsp; &nbsp; &nbsp; [ Build and Debug procedures ](doc/procedures/README.md)



# Contact
&nbsp; &nbsp; &nbsp; &nbsp;[Bugs and Issues](https://gitlab.com/lysmarine/lysmarine/issues)

&nbsp; &nbsp; &nbsp; &nbsp;[Facebook](https://www.facebook.com/lysmarineOS)
