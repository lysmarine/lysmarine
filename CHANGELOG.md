### ===== 0.8.0 =====
 - Original release

### ===== 0.8.1 =====
 - Regroup all web services under /var/www/
 - Global install of the npm package
 - Add conky
 - Disable RDP. See (https://gitlab.com/lysmarine/lysmarine/issues/23)
 - Improve stage organisation in the build script
 - Improve documentation

### ===== 0.8.2 =====
 - Add create_ap to support wifi access point mode (CLI only)
 - Fix pypilot & add menu entry.
 - Switch form wicd to nm
 - Rename x11vnc service to vnc
 - Improve documentation
